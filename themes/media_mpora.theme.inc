<?php

/**
 * @file media_mpora/themes/media_mpora.theme.inc
 *
 * Theme and preprocess functions for Media: Mpora.
 */

/**
 * Preprocess function for theme('media_mpora_video').
 */
function media_mpora_preprocess_media_mpora_video(&$variables) {
  // Build the URI.
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  $parts = $wrapper->get_parameters();
  $variables['video_id'] = check_plain($parts['v']);

  // Make the file object available.
  $file_object = file_uri_to_object($variables['uri']);

  // Add some options as their own template variables.
  foreach (array('width', 'height') as $themevar) {
    $variables[$themevar] = $variables['options'][$themevar];
  }

  $variables['url'] = url('http://mpora.com/videos/'. $variables['video_id'] . '/embed', array('external' => TRUE));

  // Do something useful with the overridden attributes from the file
  // object. We ignore alt and style for now.
  if (isset($variables['options']['attributes']['class'])) {
    if (is_array($variables['options']['attributes']['class'])) {
      $variables['classes_array'] = array_merge($variables['classes_array'], $variables['options']['attributes']['class']);
    }
    else {
      // Provide nominal support for Media 1.x.
      $variables['classes_array'][] = $variables['options']['attributes']['class'];
    }
  }

  // Add template variables for accessibility.
  $variables['title'] = check_plain($file_object->filename);
}