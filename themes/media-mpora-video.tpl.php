<?php

/**
 * @file media_mpora/includes/themes/media-mpora-video.tpl.php
 *
 * Template file for theme('media_mpora_video').
 *
 * Variables available:
 *  $uri - The uri to the Mpora video (e.g. mpora://v/AAdwq1eto1ef)
 *  $video_id - The unique identifier of the Mpora video (e.g. AAdwq1eto1ef).
 *  $id - The file entity ID (fid).
 *  $url - The full url.
 *  $width - The width to render.
 *  $height - The height to render.
 *  $title - The file's title.
 */
?>
<div class="<?php print $classes ?> media-mpora-<?php print $id; ?>">
  <iframe class="media-mpora-player" src="<?php print $url ?>" width="<?php print $width ?>" height="<?php print $height ?>" allowfullscreen frameborder="0" scrolling="no"></iframe>
</div>
