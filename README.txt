
-- INSTALLATION --

Go to Modules > enable Media: Mpora.

-- CONFIGURATION --

You can find all Media settings relating to Mpora in
Configuration > Media > File types > Video > manage file display

-- FEATURES --

Works with
* embed codes (e.g. [mpora id=1470], [mpora id=1817 lang=en])
* iframe embed code (e.g. <iframe src="http://embed.ted.com/talks/jake_barton_the_museum_of_you.html" width="560" height="315" frameborder="0" scrolling="no" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>)
* direct video URLs (e.g. http://www.ted.com/talks/apollo_robbins_the_art_of_misdirection.html)

To get the embed code:
1. Visit Mpora and locate the video you would like to embed.
2. Click on the Embed button at the bottom of the player window.
3. Copy the WordPress.com shortcode from the overlay window and paste it in your select media overlay under the web tab.


