<?php

/**
 * @file
 * File formatters for Mpora videos.
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_mpora_file_formatter_info() {
  $formatters['media_mpora_video'] = array(
    'label' => t('Mpora Video'),
    'file types' => array('video'),
    'default settings' => array(
      'width' => 640,
      'height' => 360,
    ),
    'view callback' => 'media_mpora_file_formatter_video_view',
    'settings callback' => 'media_mpora_file_formatter_video_settings',
    'mime types' => 'video/mpora',
  );

  $formatters['media_mpora_image'] = array(
    'label' => t('Mpora Preview Image'),
    'file types' => array('video'),
    'default settings' => array(
      'image_style' => '',
    ),
    'view callback' => 'media_mpora_file_formatter_image_view',
    'settings callback' => 'media_mpora_file_formatter_image_settings',
    'mime types' => 'video/mpora',
  );

  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_mpora_file_formatter_video_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);

  if ($scheme == 'mpora') {
    $element = array(
      '#theme' => 'media_mpora_video',
      '#uri' => $file->uri,
      '#options' => array(),
    );

    $display['settings']['attributes'] = array();

    foreach (array('width', 'height') as $setting) {
      $element['#options'][$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }

    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_mpora_file_formatter_video_settings($form, &$form_state, $settings) {
  $element = array();

  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
  );
  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height'],
  );

  return $element;
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_mpora_file_formatter_image_settings($form, &$form_state, $settings) {
  $element = array();

  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  );

  return $element;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_mpora_file_formatter_image_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);
  if ($scheme == 'mpora') {
    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    $image_style = $display['settings']['image_style'];
    $valid_image_styles = image_style_options(FALSE);

    if (empty($image_style) || !isset($valid_image_styles[$image_style])) {
      $element = array(
        '#theme' => 'image',
        '#path' => str_replace('http:', '', $wrapper->getOriginalThumbnailPath()),
        '#alt' => isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename,
      );
    }
    else {
      $element = array(
        '#theme' => 'image_style',
        '#style_name' => $image_style,
        '#path' => $wrapper->getLocalThumbnailPath(),
        '#alt' => isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename,
      );
    }

    return $element;
  }
}
